using Clang
using Clang.Generators
using VisRTX_jll
using ANARI_SDK_jll

cd(@__DIR__)

include_dir = joinpath(ANARI_SDK_jll.artifact_dir,"include","anari")

#TODO: may need to also generate from libanari.so and others

headers = ["anari.h",
           # visrtx doesn't seem to include this
           #joinpath("frontend","anari_extension_utility.h"),
          ]
headers = [joinpath(include_dir,h) for h ∈ headers]

opts = load_options(joinpath(@__DIR__,"generator.toml"))

args = get_default_args()
push!(args, "-I$include_dir")

ctx = create_context(headers, args, opts)

build!(ctx)
