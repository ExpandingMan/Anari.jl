
mutable struct Surface{G<:Geometry,M<:Material} <: Object
    device::Device
    handle::ANARISurface
    geometry::G
    material::M

    function Surface{G,M}(dev::Device, h::ANARISurface, g::G, m::M) where {G<:Geometry,M<:Material}
        finalizer(new{G,M}(dev, h, g, m)) do σ
            anariRelease(dev.handle, σ.handle)
        end
    end
end

Device(σ::Surface) = σ.device

_disable_data_field(::Type{<:Surface}) = true

_parameter_arg_type(::Type{<:Surface}, ::Val{:geometry}) = Geometry
_parameter_input_type(::Type{<:Surface}, ::Val{:geometry}) = Geometry
_c_parameter_name(::Type{<:Surface}, ::Val{:geometry}) = "geometry"
_c_parameter_type_code(::Type{<:Surface}, ::Val{:geometry}) = ANARI_GEOMETRY

_parameter_arg_type(::Type{<:Surface}, ::Val{:material}) = Material
_parameter_input_type(::Type{<:Surface}, ::Val{:material}) = Material
_c_parameter_name(::Type{<:Surface}, ::Val{:material}) = "material"
_c_parameter_type_code(::Type{<:Surface}, ::Val{:material}) = ANARI_MATERIAL

function Surface(dev::Device, g::Geometry, m::Material; raw::Bool=false)
    h = anariNewSurface(dev.handle)
    h == C_NULL && error("failed to create surface")
    σ = Surface{typeof(g),typeof(m)}(dev, h, g, m)
    if !raw
        setparameter!(σ, Val{:geometry}(), g; commit=false)
        setparameter!(σ, Val{:material}(), m; commit=false)
        _commitparams!(σ)
    end
    σ
end
Surface(g::Geometry, m::Material; raw::Bool=false) = Surface(anariglobal(Device), g, m; raw)
