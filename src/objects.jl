
abstract type Object end

_ctypecode(::Type{<:Object}) = ANARI_UNKNOWN

# some julia types that we need type codes for... not necessarily complete
_ctypecode(::Type{Bool}) = ANARI_BOOL
_ctypecode(::Type{Float16}) = ANARI_FLOAT16
_ctypecode(::Type{Float32}) = ANARI_FLOAT32
_ctypecode(::Type{Float64}) = ANARI_FLOAT64
_ctypecode(::Type{String}) = ANARI_STRING
_ctypecode(::Type{Vector{String}}) = ANARI_STRING_LIST
_ctypecode(::Type{RGBA{N0f8}}) = ANARI_UFIXED8_RGBA_SRGB

_commitparams!(obj::Object) = anariCommitParameters(Device(obj).handle, obj.handle)

Base.show(io::IO, obj::Object) = _defaultshow(io, obj, ())

Base.convert(::Type{O}, o::O) where {O<:Object} = o

#WARN: this is behaving like it works but is not really tested!

_c_param_element(x) = x
_c_param_element(::AbstractArray) = error("tried to pass array as parameter element")
_c_param_element(x::Object)  = x.handle

# this *should* "just work" for colors
function _map_param_vector!(obj::Object, name::AbstractString, ::Type{T}, n::Integer, code, itr,
                           ) where {T}
    ptr = anariMapParameterArray1D(Device(obj).handle, obj.handle, name,
                                   code, n, Ref(UInt64(1)),
                                  )
    # use array instead of unsafe_store! here so that bounds checking works properly
    a = unsafe_wrap(Array, convert(Ptr{T}, ptr), n)
    for (i, x) ∈ enumerate(itr)
        a[i] = _c_param_element(x)
    end
    anariUnmapParameterArray(Device(obj).handle, obj.handle, name)
end

# this is Nothing, Vector, or Matrix depending on the container type
_parameter_container_type(::Type{<:Object}, ::Val) = Nothing

_parameter_scalar_ref(x::Array) = x
_parameter_scalar_ref(x) = Ref(x)
_parameter_scalar_ref(x::Object)  = Ref(x.handle)

function _setparameter!(obj::O, v::Val, ::Type{Nothing}, x) where {O<:Object}
    x = _parameter_input_convert(O, v, x)::_parameter_input_type(O, v)
    name = _c_parameter_name(O, v)
    code = _c_parameter_type_code(O, v, x)
    GC.@preserve x name begin
        #WARN: are we absolutely certain we can free the memory after we do `anariSetParameter`??
        anariSetParameter(Device(obj).handle, obj.handle, name, code, _parameter_scalar_ref(x))
    end
end

_parameter_iterator(x) = Iterators.flatten(x)
_parameter_iterator(x::AbstractVector) = x

function _setparameter!(obj::O, v::Val, ::Type{Vector}, x) where {O<:Object}
    name = _c_parameter_name(O, v)
    code = _c_parameter_eltype_code(O, v)
    itr = _parameter_iterator(x)
    GC.@preserve name begin
        _map_param_vector!(obj, name, _parameter_input_eltype(O, v), length(itr), code, itr)
    end
end

# required
function _parameter_arg_type end

_parameter_checks(::Type{<:Object}, ::Val, x) = nothing

_c_parameter_type_code(::Type{O}, v::Val) where {O} = _ctypecode(_parameter_input_type(O, v))
# this is a fallback if the method was not defined with the specific input
_c_parameter_type_code(::Type{O}, v::Val, x) where {O<:Object} = _c_parameter_type_code(O, v)

_parameter_input_convert(::Type{O}, v::Val, x) where {O} = convert(_parameter_input_type(O, v), x)

_disable_data_field(::Type{<:Object}) = false

getparameter(obj::Object, v::Val{name}) where {name} = getfield(obj.data, name)
getparameter(obj::Object, name::Symbol) = getparameter(obj, Val{name}())

function _setparameter!(obj::O, v::Val{name}, x; commit::Bool=true) where {O<:Object,name}
    _setparameter!(obj, v, _parameter_container_type(O, v), x)
    commit && _commitparams!(obj)
    nothing
end

function setparameter!(obj::Object, v::Val{name}; commit::Bool=true) where {name}
    _setparameter!(obj, v, getfield(obj.data, name); commit)
end
setparameter!(obj::Object, name::Symbol; commit::Bool=true) = setparameter!(obj, Val{name}(); commit)

function setparameter!(obj::O, v::Val{name}, x; commit::Bool=true) where {O<:Object,name}
    T = _parameter_arg_type(O, v)
    if !(x isa T)
        throw(ArgumentError("parameter $name must be of type $T, got $(typeof(x))"))
    end
    _parameter_checks(O, v, x)
    if !_disable_data_field(O)
        ftype = _non_nothing_type(fieldtype(typeof(obj.data), name))
        setfield!(obj.data, name, convert(ftype, x))
    end
    _setparameter!(obj, v, x; commit)
end
setparameter!(obj::Object, name::Symbol, x; commit::Bool=true) = setparameter!(obj, Val{name}(), x; commit)

# this is used for parameters that have finitely many discrete values; must be assigned explicitly
function _parameter_check_valid_option(::Type{O}, v::Val{name}, x) where {O<:Object, name}
    opts = _parameter_valid_options(O, v)
    if x ∉ opts
        throw(ArgumentError("valid options for $name are $opts; got $x"))
    end
end


abstract type ObjectData end

_getfield(data::ObjectData, ::Val{name}) where {name} = getfield(data, name)

function setdata!(obj::Object, v::Val{name}, data::ObjectData; commit::Bool=true) where {name}
    x = _getfield(data, v)
    # in all cases, nothing indicates do not set
    isnothing(x) || setparameter!(obj, v, _getfield(data, v); commit)
    nothing
end

#TODO: this probably has to be generated somehow to avoid run-time dispatch
function setalldata!(obj::Object, data::ObjectData)
    for name ∈ fieldnames(typeof(data))
        setdata!(obj, Val{name}(), data; commit=false)
    end
    _commitparams!(obj)
    data
end


