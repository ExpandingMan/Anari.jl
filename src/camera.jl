
abstract type Camera <: Object end

_default_perspective_position() = @SVector zeros(Float32,3)
_default_perspective_direction() = @SVector Float32[0,0,-1]
_default_perspective_up() = @SVector Float32[0,1,0]

@kwdef mutable struct PerspectiveData <: ObjectData
    #common:
    position::SVector{3,Float32} = _default_perspective_position()
    direction::SVector{3,Float32} = _default_perspective_direction()
    up::SVector{3,Float32} = _default_perspective_up()
    #TODO: image region

    #perspective:
    field_of_view::Float32 = π/3
    aspect_ratio::Float32 = 1
    clip_plane_near::Union{Nothing,Float32} = nothing
    clip_plane_far::Union{Nothing,Float32} = nothing
end

_ctypecode(::Type{<:Camera}) = ANARI_CAMERA


mutable struct PerspectiveCamera <: Camera
    device::Device
    handle::ANARICamera
    data::PerspectiveData

    function PerspectiveCamera(dev::Device, h::ANARICamera, data::PerspectiveData=PerspectiveData())
        finalizer(new(dev, h, data)) do c
            #@debug("destroying Camera")
            anariRelease(dev.handle, c.handle)
        end
    end
end

associatedtype(::Type{<:PerspectiveData}) = PerspectiveCamera

Device(c::PerspectiveCamera) = c.device

subtype(c::PerspectiveCamera) = c.subtype


_parameter_arg_type(::Type{<:Camera}, ::Val{:position}) = AbstractVector{<:Real}
_parameter_input_type(::Type{<:Camera}, ::Val{:position}) = Vector{Float32}
function _parameter_checks(::Type{<:Camera}, ::Val{:position}, x)
    length(x) == 3 || throw(ArgumentError("ANARI only supports 3 spatial dimensions"))
end
_c_parameter_name(::Type{<:Camera}, ::Val{:position}) = "position"
_c_parameter_type_code(::Type{<:Camera}, ::Val{:position}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:Camera}, ::Val{:direction}) = AbstractVector{<:Real}
_parameter_input_type(::Type{<:Camera}, ::Val{:direction}) = Vector{Float32}
function _parameter_checks(::Type{<:Camera}, ::Val{:direction}, x)
    length(x) == 3 || throw(ArgumentError("ANARI only supports 3 spatial dimensions"))
end
_c_parameter_name(::Type{<:Camera}, ::Val{:direction}) = "direction"
_c_parameter_type_code(::Type{<:Camera}, ::Val{:direction}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:Camera}, ::Val{:up}) = AbstractVector{<:Real}
_parameter_input_type(::Type{<:Camera}, ::Val{:up}) = Vector{Float32}
function _parameter_checks(::Type{<:Camera}, ::Val{:up})
    length(x) == 3 || throw(ArgumentError("ANARI only supports 3 spatial dimensions"))
end
_c_parameter_name(::Type{<:Camera}, ::Val{:up}) = "up"
_c_parameter_type_code(::Type{<:Camera}, ::Val{:up}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:PerspectiveCamera}, ::Val{:field_of_view}) = Real
_parameter_input_type(::Type{<:PerspectiveCamera}, ::Val{:field_of_view}) = Float32
function _parameter_checks(::Type{<:PerspectiveCamera}, x)
    x > 0 || throw(ArgumentError("camera field of view must be > 0"))
end
_c_parameter_name(::Type{<:PerspectiveCamera}, ::Val{:field_of_view}) = "fovy"

_parameter_arg_type(::Type{<:PerspectiveCamera}, ::Val{:aspect_ratio}) = Real
_parameter_input_type(::Type{<:PerspectiveCamera}, ::Val{:aspect_ratio}) = Float32
function _parameter_checks(::Type{<:PerspectiveCamera}, ::Val{:aspect_ratio}, x)
    x > 0 || throw(ArgumentError("camera aspect ratio must be > 0")) 
end
_c_parameter_name(::Type{<:PerspectiveCamera}, ::Val{:aspect_ratio}) = "aspect"

_parameter_arg_type(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_near}) = Real
_parameter_input_type(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_near}) = Float32
function _parameter_checks(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_near}, x)
    x > 0 || throw(ArgumentError("clip plane distance must be > 0"))
end
_c_parameter_name(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_near}) = "near"

_parameter_arg_type(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_far}) = Real
_parameter_input_type(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_far}) = Float32
function _parameter_checks(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_far}, x)
    x > 0 || throw(ArgumentError("clip plane distance must be > 0"))
end
_c_parameter_name(::Type{<:PerspectiveCamera}, ::Val{:clip_plane_far}) = "far"


for (param, fname, fname!) ∈ [(:(:position), :position, :position!),
                              (:(:direction), :direction, :direction!),
                              (:(:up), :up, :up!),
                             ]
    @eval $fname(c::Camera) = getparameter(c, Val{$param}())
    @eval function $fname!(c::Camera, x::$(_parameter_arg_type(Camera, Val{param.value}())); commit::Bool=true)
        setparameter!(c, Val{$param}(), x; commit)
        getparameter(c, Val{$param}())
    end
end


function PerspectiveCamera(dev::Device, x::AbstractVector{<:Real}=_default_perspective_position(),
                           n::AbstractVector{<:Real}=_default_perspective_direction(),
                           u::AbstractVector{<:Real}=_default_perspective_up();
                           raw::Bool=false,
                           kw...
                          )
    h = anariNewCamera(dev.handle, "perspective")
    h == C_NULL && error("failed to create camera")
    data = PerspectiveData(;position=x, direction=n, up=u, kw...)
    c = PerspectiveCamera(dev, h, data)
    raw || setalldata!(c, data)
    c
end
function PerspectiveCamera(x::AbstractVector{<:Real}=_default_perspective_position(),
                           n::AbstractVector{<:Real}=_default_perspective_direction(),
                           u::AbstractVector{<:Real}=_default_perspective_up();
                           kw...
                          )
    PerspectiveCamera(anariglobal(Device), x, n, u; kw...)
end

translate!(c::Camera, Δx::AbstractVector{<:Real}) = position!(c, position(c) .+ Δx)

function rotate!(𝒻, c::Camera)
    up!(c, 𝒻(up(c)))
    direction!(c, 𝒻(direction(c)))
end
