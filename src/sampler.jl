
abstract type Sampler <: Object end

_ctypecode(::Type{<:Sampler}) = ANARI_SAMPLER

@kwdef mutable struct Image1DData <: ObjectData
    #TODO: incomplete

    image::Union{Nothing,Vector{RGBA{N0f8}}} = nothing
    filter::Symbol = :linear
    wrap_mode::Symbol = :clamp_to_edge
end

mutable struct Image1D <: Sampler
    device::Device
    handle::ANARISampler
    data::Image1DData

    function Image1D(dev::Device, h::ANARISampler, data::Image1DData=Image1DData())
        finalizer(new(dev, h, data)) do img
            anariRelease(dev.handle, img.handle)
        end
    end
end

associatedtype(::Type{<:Image1DData}) = Image1D

Device(img::Image1D) = img.device

subtype(img::Image1D) = :image1D


_parameter_arg_type(::Type{<:Image1D}, ::Val{:image}) = AbstractVector{<:Colorant}
_parameter_input_type(::Type{<:Image1D}, ::Val{:image}) = Vector{RGBA{Nf08}}
_c_parameter_name(::Type{<:Image1D}, ::Val{:image}) = "image"
_parameter_container_type(::Type{<:Image1D}, ::Val{:image}) = Vector
_parameter_input_eltype(::Type{<:Image1D}, ::Val{:image}) = RGBA{N0f8}
_c_parameter_eltype_code(::Type{<:Image1D}, ::Val{:image}) = _ctypecode(RGBA{N0f8})

_parameter_arg_type(::Type{<:Image1D}, ::Val{:filter}) = Symbol
_parameter_input_type(::Type{<:Image1D}, ::Val{:filter}) = String
_parameter_valid_options(::Type{<:Image1D}, ::Val{:filter}) = (:linear, :nearest)
_parameter_checks(::Type{<:Image1D}, v::Val{:filter}, x) = _parameter_check_valid_option(Image1D, v, x)
_c_parameter_name(::Type{<:Image1D}, ::Val{:filter}) = "filter"
_parameter_input_convert(::Type{<:Image1D}, ::Val{:filter}, x) = string(x)

_parameter_arg_type(::Type{<:Image1D}, ::Val{:wrap_mode}) = Symbol
_parameter_input_type(::Type{<:Image1D}, ::Val{:wrap_mode}) = String
_parameter_valid_options(::Type{<:Image1D}, ::Val{:wrap_mode}) = (:clamp_to_edge, :repeat, :mirror_repeat)
_parameter_checks(::Type{<:Image1D}, v::Val{:wrap_mode}, x) = _parameter_check_valid_option(Image1D, v, x)
_c_parameter_name(::Type{<:Image1D}, ::Val{:wrap_mode}) = "wrapMode"
function _parameter_input_convert(::Type{<:Image1D}, ::Val{:wrap_mode}, x)
    if x == :clamp_to_edge
        "clampToEdge"
    elseif x == :repeat
        "repeat"
    elseif x == :mirror_repeat
        "mirrorRepeat"
    else
        throw(ArgumentError("got invalid parameter somewhere that should be unreachable"))
    end
end


function Image1D(dev::Device, v::Union{Nothing,AbstractVector{<:Colorant}}=nothing; kw...)
    h = anariNewSampler(dev.handle, "image1D")
    h == C_NULL && error("failed to create sampler")
    data = Image1DData(;image=v, kw...)
    img = Image1D(dev, h, data)
    setalldata!(img, data)
    img
end
Image1D(v::Union{Nothing,AbstractVector{<:Colorant}}=nothing; kw...) = Image1D(anariglobal(Device), v; kw...)
