
mutable struct Library
    handle::ANARILibrary
    name::Symbol

    function Library(h::Ptr, name::Union{AbstractString,Symbol})
        finalizer(new(h, Symbol(name))) do l
            #@debug("unloading library $name")
            anariUnloadLibrary(l.handle)
        end
    end
end

function Library(name::Symbol)
    callback = @cfunction(status_callback, Cvoid,
                          (Ptr{Cvoid}, ANARIDevice, ANARIObject, ANARIDataType, ANARIStatusSeverity,
                           ANARIStatusCode, Cstring)
                         )
    Library(anariLoadLibrary(name, callback, C_NULL), name)
end

_ctypecode(::Type{Library}) = ANARI_LIBRARY

Base.show(io::IO, lib::Library) = _defaultshow(io, lib, (:name,))

const LIBRARY = RefValue{Library}()

anariglobal(::Type{Library}) = LIBRARY[]

function anariglobal!(lib::Library) 
    LIBRARY[] = lib
    GC.gc()  # forcefully destroy old lib
    nothing
end

devicesubtypes(lib::Library) = Symbol.(_unsafe_load_string_array(anariGetDeviceSubtypes(lib.handle)))
devicesubtypes() = devicesubtypes(anariglobal(Library))

function defaultdevicesubtype(lib::Library) 
    st = devicesubtypes(lib)
    isempty(st) && error("no device subtypes found for library $lib")
    first(devicesubtypes(lib))
end

mutable struct Device
    lib::Library
    handle::ANARIDevice
    type::Symbol

    function Device(lib::Library, h::Ptr, type::Union{AbstractString,Symbol})
        finalizer(new(lib, h, Symbol(type))) do d
            #@debug("destroying Device")
            anariRelease(d.handle, d.handle)
        end
    end
end

_ctypecode(::Type{Device}) = ANARI_DEVICE

const DEVICE = RefValue{Device}()

anariglobal(::Type{Device}) = DEVICE[]

function anariglobal!(dev::Device) 
    DEVICE[] = dev
    GC.gc()  # forcefully destroy old device
    nothing
end

Device(lib::Library, type::Symbol=defaultdevicesubtype(lib)) = Device(lib, anariNewDevice(lib.handle, type), type)
Device(type::Symbol) = Device(anariglobal(Library), type)
Device() = Device(anariglobal(Library), defaultdevicesubtype(anariglobal(Library)))

Base.show(io::IO, dev::Device) = _defaultshow(io, dev, (:type,))

function objectsubtypes(dev::Device, ::Type{T}) where {T<:Object}
    Symbol.(_unsafe_load_string_array(anariGetObjectSubtypes(dev.handle, _ctypecode(T))))
end
objectsubtypes(::Type{T}) where {T<:Object} = objectsubtypes(anariglobal(Device), T)

defaultsubtype(dev::Device, ::Type{T}) where {T<:Object} = (first ∘ objectsubtypes)(dev, T)
defaultsubtype(::Type{T}) where {T<:Object} = defaultsubtype(anariglobal(Device), T)

objectsubtypes(obj::Object) = objectsubtypes(Device(obj), typeof(obj))

function description(dev::Device, ::Type{T}, subtype::Symbol=defaultsubtype(dev, T)) where {T<:Object}
    ptr = anariGetObjectInfo(dev.handle, _ctypecode(T), string(subtype), "description", ANARI_STRING)
    ptr == C_NULL ? "" : unsafe_string(convert(Ptr{Cchar}, ptr))
end
description(::Type{T}, subtype::Symbol=defaultsubtype(T)) where {T<:Object}  = description(anariglobal(Device), T, subtype)

description(obj::Object) = description(Device(obj), typeof(obj), subtype(obj))

function _parameters(dev::Device, ::Type{T}, subtype::Symbol=defaultsubtype(dev, T)) where {T<:Object}
    ptr = anariGetObjectInfo(dev.handle, _ctypecode(T), string(subtype), "parameter", ANARI_PARAMETER_LIST)
    ps = _unsafe_load_param_array(ptr)
    map(x -> (name=unsafe_string(x.name), type=x.type), ps)
end
_parameters(::Type{T}, subtype::Symbol=defaultsubtype(T)) where {T<:Object} = _parameters(anariglobal(Device), T, subtype)

