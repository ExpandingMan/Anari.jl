
abstract type Light <: Object end

_ctypecode(::Type{<:Light}) = ANARI_LIGHT

Device(l::Light) = l.device

Base.show(io::IO, l::Light) = _defaultshow(io, l, ())


@kwdef mutable struct DirectionalLightData <: ObjectData
    #common:
    color::RGB{Float32} = RGB{Float32}(1, 1, 1)

    #directional:
    direction::SVector{3,Float32} = @SVector Float32[0, 0, -1]
    irradiance::Float32 = 1
    angular_diameter::Float32 = 0
    radiance::Union{Nothing,Float32} = nothing
end

mutable struct DirectionalLight <: Light
    device::Device
    handle::ANARILight
    data::DirectionalLightData

    function DirectionalLight(dev::Device, h::ANARILight, data::DirectionalLightData=DirectionalLightData())
        finalizer(new(dev, h, data)) do l
            anariRelease(dev.handle, l.handle)
        end
    end
end

associatedtype(::Type{<:DirectionalLightData}) = DirectionalLight

subtype(l::DirectionalLight) = :directional

_parameter_arg_type(::Type{<:DirectionalLight}, ::Val{:color}) = Colorant
_parameter_input_type(::Type{<:DirectionalLight}, ::Val{:color}) = Vector{Float32}
_c_parameter_name(::Type{<:DirectionalLight}, ::Val{:color}) = "color"
_c_parameter_type_code(::Type{<:DirectionalLight}, ::Val{:color}) = ANARI_FLOAT32_VEC3
function _parameter_input_convert(::Type{<:DirectionalLight}, ::Val{:color}, c::Colorant)
    c = convert(RGB{Float32}, c)
    Float32[c.r, c.g, c.b]
end

_parameter_arg_type(::Type{<:DirectionalLight}, ::Val{:direction}) = AbstractVector{<:Real}
_parameter_input_type(::Type{<:DirectionalLight}, ::Val{:direction}) = Vector{Float32}
function _parameter_checks(::Type{<:DirectionalLight}, ::Val{:direction}, x)
    length(x) == 3 || throw(ArgumentError("ANARI only supports 3 spatial dimensions"))
end
_c_parameter_name(::Type{<:DirectionalLight}, ::Val{:direction}) = "direction"
_c_parameter_type_code(::Type{<:DirectionalLight}, ::Val{:direction}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:DirectionalLight}, ::Val{:irradiance}) = Real
_parameter_input_type(::Type{<:DirectionalLight}, ::Val{:irradiance}) = Float32
function _parameter_checks(::Type{<:DirectionalLight}, ::Val{:irradiance}, x)
    x ≥ 0 || throw(ArgumentError("irradiance must be ≥ 0"))
end
_c_parameter_name(::Type{<:DirectionalLight}, ::Val{:irradiance}) = "irradiance"

_parameter_arg_type(::Type{<:DirectionalLight}, ::Val{:angular_diameter}) = Real
_parameter_input_type(::Type{<:DirectionalLight}, ::Val{:angular_diameter}) = Float32
function _parameter_checks(::Type{<:DirectionalLight}, ::Val{:angular_diameter}, x)
    x ≥ 0 || throw(ArgumentError("angular diameter must be ≥ 0"))
end
_c_parameter_name(::Type{<:DirectionalLight}, ::Val{:angular_diameter}) = "angularDiameter"

_parameter_arg_type(::Type{<:DirectionalLight}, ::Val{:radiance}) = Real
_parameter_input_type(::Type{<:DirectionalLight}, ::Val{:radiance}) = Float32
function _parameter_checks(::Type{<:DirectionalLight}, ::Val{:radiance}, x)
    x ≥ 0 || throw(ArgumentError("radiance must be ≥ 0"))
end
_c_parameter_name(::Type{<:DirectionalLight}, ::Val{:radiance}) = "radiance"


function DirectionalLight(dev::Device, n::AbstractVector{<:Real}=@SVector(Float32[0,0,-1]); kw...)
    h = anariNewLight(dev.handle, "directional")
    h == C_NULL && error("failed to create light")
    data = DirectionalLightData(;direction=n, kw...)
    l = DirectionalLight(dev, h, data)
    setalldata!(l, data)
    l
end
function DirectionalLight(n::AbstractVector{<:Real}=@SVector(Float32[0,0,-1]); kw...) 
    DirectionalLight(anariglobal(Device), n; kw...)
end


@kwdef mutable struct PointLightData <: ObjectData
    position::SVector{3,Float32} = @SVector(zeros(Float32,3))
    intensity::Union{Nothing,Float32} = 1
    power::Float32 = 1
end

mutable struct PointLight <: Light
    device::Device
    handle::ANARILight
    data::PointLightData

    function PointLight(dev::Device, h::ANARILight, data::PointLightData=PointLightData())
        finalizer(new(dev, h, data)) do l
            anariRelease(dev.handle, l.handle)
        end
    end
end

associatedtype(::Type{<:PointLightData}) = PointLight

subtype(l::PointLight) = :point

_parameter_arg_type(::Type{<:PointLight}, ::Val{:position}) = AbstractVector{<:Real}
_parameter_input_type(::Type{<:PointLight}, ::Val{:position}) = Vector{Float32}
function _parameter_checks(::Type{<:PointLight}, ::Val{:position}, x)
    length(x) == 3 || throw(ArgumentError("ANARI only supports 3 spatial dimensions"))
end
_c_parameter_name(::Type{<:PointLight}, ::Val{:position}) = "position"
_c_parameter_type_code(::Type{<:PointLight}, ::Val{:position}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:PointLight}, ::Val{:intensity}) = Real
_parameter_input_type(::Type{<:PointLight}, ::Val{:intensity}) = Float32
function _parameter_checks(::Type{<:PointLight}, ::Val{:intensity}, x)
    x ≥ 0 || throw(ArgumentError("light intensity must be ≥ 0"))
end
_c_parameter_name(::Type{<:PointLight}, ::Val{:intensity}) = "intensity"
_c_parameter_type_code(::Type{<:PointLight}, ::Val{:intensity}) = ANARI_FLOAT32

_parameter_arg_type(::Type{<:PointLight}, ::Val{:power}) = Real
_parameter_input_type(::Type{<:PointLight}, ::Val{:power}) = Float32
function _parameter_checks(::Type{<:PointLight}, ::Val{:power}, x)
    x ≥ 0 || throw(ArgumentError("light power must be ≥ 0"))
end
_c_parameter_name(::Type{<:PointLight}, ::Val{:power}) = "power"
_c_parameter_type_code(::Type{<:PointLight}, ::Val{:power}) = ANARI_FLOAT32

function PointLight(dev::Device, x::AbstractVector{<:Real}=@SVector(zeros(Float32,3)); kw...)
    h = anariNewLight(dev.handle, "point")
    h == C_NULL && error("failed to create light")
    data = PointLightData(;position=x, kw...)
    l = PointLight(dev, h, data)
    setalldata!(l, data)
    l
end
function PointLight(x::AbstractVector{<:Real}=@SVector(zeros(Float32,3)); kw...)
    PointLight(anariglobal(Device), x; kw...)
end
