
abstract type Material <: Object end

_ctypecode(::Type{<:Material}) = ANARI_MATERIAL

Base.show(io::IO, m::Material) = _defaultshow(io, m, ())

@kwdef mutable struct MatteData <: ObjectData
    color::Union{Sampler,RGB{Float32}} = RGB{Float32}(0.8, 0.8, 0.8)
    opacity::Float32 = 1
    alpha_mode::Symbol = :opaque
    alpha_cutoff::Float32 = 0.5
end

mutable struct Matte <: Material
    device::Device
    handle::ANARIMaterial
    data::MatteData

    function Matte(dev::Device, h::ANARIMaterial, data::MatteData=MatteData())
        finalizer(new(dev, h, data)) do m
            anariRelease(dev.handle, m.handle)
        end
    end
end

associatedtype(::Type{<:MatteData}) = Matte

Device(m::Matte) = m.device

subtype(m::Matte) = :matte

_parameter_arg_type(::Type{<:Matte}, ::Val{:color}) = Union{Sampler,Colorant}
_parameter_input_type(::Type{<:Matte}, ::Val{:color}) = Union{Sampler,Vector{Float32}}
_c_parameter_name(::Type{<:Matte}, ::Val{:color}) = "color"
_c_parameter_type_code(::Type{<:Matte}, ::Val{:color}, ::Vector{Float32}) = ANARI_FLOAT32_VEC3
_c_parameter_type_code(::Type{<:Matte}, ::Val{:color}, ::Sampler) = ANARI_SAMPLER
function _parameter_input_convert(::Type{<:Matte}, ::Val{:color}, c::Colorant)
    c = convert(RGB{Float32}, c)
    Float32[c.r, c.g, c.b]
end
_parameter_input_convert(::Type{<:Matte}, ::Val{:color}, s::Sampler) = s

_parameter_arg_type(::Type{<:Matte}, ::Val{:opacity}) = Real
_parameter_input_type(::Type{<:Matte}, ::Val{:opacity}) = Float32
function _parameter_checks(::Type{<:Matte}, ::Val{:opacity}, x)
    x ≥ 0 || throw(ArgumentError("opacity must be ≥ 0"))
end
_c_parameter_name(::Type{<:Matte}, ::Val{:opacity}) = "opacity"

_parameter_arg_type(::Type{<:Matte}, ::Val{:alpha_mode}) = Symbol
_parameter_input_type(::Type{<:Matte}, ::Val{:alpha_mode}) = String
_parameter_valid_options(::Type{<:Matte}, ::Val{:alpha_mode}) = (:opaque, :blend, :mask)
_parameter_checks(::Type{<:Matte}, v::Val{:alpha_mode}, x) = _parameter_check_valid_option(Matte, v, x)
_c_parameter_name(::Type{<:Matte}, ::Val{:alpha_mode}) = "alphaMode"
_parameter_input_convert(::Type{<:Matte}, ::Val{:alpha_mode}, a::Symbol) = string(a)

_parameter_arg_type(::Type{<:Matte}, ::Val{:alpha_cutoff}) = Real
_parameter_input_type(::Type{<:Matte}, ::Val{:alpha_cutoff}) = Float32
function _parameter_checks(::Type{<:Matte}, ::Val{:alpha_cutoff}, x)
    x ≥ 0 || throw(ArgumentError("alpha cutoff must be ≥ 0"))
end
_c_parameter_name(::Type{<:Matte}, ::Val{:alpha_cutoff}) = "alphaCutoff"


function Matte(dev::Device; kw...)
    h = anariNewMaterial(dev.handle, "matte")
    h == C_NULL && error("failed to create material")
    data = MatteData(;kw...)
    m = Matte(dev, h, data)
    setalldata!(m, data)
    m
end
Matte(;kw...) = Matte(anariglobal(Device); kw...)


@kwdef mutable struct PhysicallyBasedData <: ObjectData
    #TODO: this is incomplete as has no sampler stuff!

    base_color::RGB{Float32} = RGB{Float32}(1,1,1)
    opacity::Float32 = 1
    metallic::Float32 = 1
    roughness::Float32 = 1
    emissive::RGB{Float32} = RGB{Float32}(0,0,0)
    alpha_mode::Symbol = :opaque
    alpha_cutoff::Float32 = 0.5
    specular::Float32 = 0
    specular_color::RGB{Float32} = RGB{Float32}(1,1,1)
    clearcoat::Float32 = 0
    clearcoat_roughness::Float32 = 0
    transmission::Float32 = 0
    index_of_refraction::Float32 = 1.5
    thickness::Float32 = 0
    attenuation_distance::Float32 = Inf
    attenuation_color::RGB{Float32} = RGB{Float32}(1,1,1)
    sheen_color::RGB{Float32} = RGB{Float32}(0,0,0)
    sheen_roughness::Float32 = 0
    iridescence::Float32 = 0
    iridescence_index_of_refraction::Float32 = 1.3
    iridescence_thickness::Float32 = 0
end

mutable struct PhysicallyBased <: Material
    device::Device
    handle::ANARIMaterial
    data::PhysicallyBasedData

    function PhysicallyBased(dev::Device, h::ANARIMaterial, data::PhysicallyBasedData=PhysicallyBasedData())
        finalizer(new(dev, h, data)) do m
            anariRelease(dev.handle, m.handle)
        end
    end
end

associatedtype(::Type{<:PhysicallyBasedData}) = PhysicallyBased

Device(m::PhysicallyBased) = m.device

subtype(m::PhysicallyBased) = :physicallyBased

# Float32 params
for (param, cname) ∈ [(:(:opacity), "opacity"), (:(:metallic), "metallic"), (:(:roughness), "roughness"),
                      (:(:alpha_cutoff), "alphaCutoff"), (:(:specular), "specular"),
                      (:(:clearcoat), "clearcoat"), (:(:clearcoat_roughness), "clearcoatRoughness"),
                      (:(:transmission), "transmission"), (:(:index_of_refraction), "ior"),
                      (:(:thickness), "thickness"), (:(:attenuation_distance), "attenuationDistance"),
                      (:(:sheen_roughness), "sheenRoughness"), (:(:iridescence), "iridescence"),
                      (:(:iridescence_index_of_refraction), "iridescenceIor"),
                      (:(:iridescence_thickness), "iridescenceThickness"),
                     ]
    @eval begin
        _parameter_arg_type(::Type{<:PhysicallyBased}, ::Val{$param}) = Real
        _parameter_input_type(::Type{<:PhysicallyBased}, ::Val{$param}) = Float32
        function _parameter_checks(::Type{<:PhysicallyBased}, ::Val{$param}, x)
            cname = $cname
            x ≥ 0 || throw(ArgumentError("$cname must be ≥ 0"))
        end
        _c_parameter_name(::Type{<:PhysicallyBased}, ::Val{$param}) = $cname
    end
end

# Color params
for (param, cname) ∈ [(:(:base_color), "baseColor"), (:(:emissive), "emissive"),
                      (:(:specular_color), "specularColor"), (:(:attenuation_color), "attenuationColor"),
                      (:(:sheen_color), "sheenColor"),
                     ]
    @eval begin
        _parameter_arg_type(::Type{<:PhysicallyBased}, ::Val{$param}) = Colorant
        _parameter_input_type(::Type{<:PhysicallyBased}, ::Val{$param}) = Vector{Float32}
        _c_parameter_name(::Type{<:PhysicallyBased}, ::Val{$param}) = $cname
        _c_parameter_type_code(::Type{<:PhysicallyBased}, ::Val{$param}) = ANARI_FLOAT32_VEC3
        function _parameter_input_convert(::Type{<:PhysicallyBased}, ::Val{$param}, c::Colorant)
            c = convert(RGB{Float32}, c)
            Float32[c.r, c.g, c.b]
        end
    end
end

_parameter_arg_type(::Type{<:PhysicallyBased}, ::Val{:alpha_mode}) = Symbol
_parameter_input_type(::Type{<:PhysicallyBased}, ::Val{:alpha_mode}) = String
_parameter_valid_options(::Type{<:PhysicallyBased}, ::Val{:alpha_mode}) = (:opaque, :blend, :mask)
function _parameter_checks(::Type{<:PhysicallyBased}, v::Val{:alpha_mode}, x) 
    _parameter_check_valid_option(PhysicallyBased, v, x)
end
_c_parameter_name(::Type{<:PhysicallyBased}, ::Val{:alpha_mode}) = "alphaMode"
_parameter_input_convert(::Type{<:PhysicallyBased}, ::Val{:alpha_mode}, a::Symbol) = string(a)

function PhysicallyBased(dev::Device; kw...)
    h = anariNewMaterial(dev.handle, "physicallyBased")
    h == C_NULL && error("failed to create material")
    data = PhysicallyBasedData(;kw...)
    m = PhysicallyBased(dev, h, data)
    setalldata!(m, data)
    m
end
PhysicallyBased(;kw...) = PhysicallyBased(anariglobal(Device); kw...)
