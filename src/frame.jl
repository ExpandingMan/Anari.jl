
@kwdef mutable struct FrameData <: ObjectData
    world::World
    camera::Camera
    renderer::Renderer
    size::Tuple{Int,Int} = (512, 512)
    accumulation::Bool = false
end


mutable struct Frame{C<:Colorant} <: Object
    device::Device
    handle::ANARIFrame
    data::FrameData

    function Frame{C}(dev::Device, h::ANARIFrame, data::FrameData) where {C<:Colorant}
        finalizer(new{C}(dev, h, data)) do f
            anariRelease(dev.handle, f.handle)
        end
    end
end

_ctypecode(::Type{<:Frame}) = ANARI_FRAME

associatedtype(::Type{<:FrameData}) = Frame

Device(f::Frame) = f.device


_parameter_arg_type(::Type{<:Frame}, ::Val{:world}) = World
_parameter_input_type(::Type{<:Frame}, ::Val{:world}) = World
_c_parameter_name(::Type{<:Frame}, ::Val{:world}) = "world"
_c_parameter_type_code(::Type{<:Frame}, ::Val{:world}) = ANARI_WORLD

_parameter_arg_type(::Type{<:Frame}, ::Val{:renderer}) = Renderer
_parameter_input_type(::Type{<:Frame}, ::Val{:renderer}) = Renderer
_c_parameter_name(::Type{<:Frame}, ::Val{:renderer}) = "renderer"
_c_parameter_type_code(::Type{<:Frame}, ::Val{:renderer}) = ANARI_RENDERER

_parameter_arg_type(::Type{<:Frame}, ::Val{:camera}) = Camera
_parameter_input_type(::Type{<:Frame}, ::Val{:camera}) = Camera
_c_parameter_name(::Type{<:Frame}, ::Val{:camera}) = "camera"
_c_parameter_type_code(::Type{<:Frame}, ::Val{:camera}) = ANARI_CAMERA

_parameter_arg_type(::Type{<:Frame}, ::Val{:size}) = Tuple{<:Integer,<:Integer}
_parameter_input_type(::Type{<:Frame}, ::Val{:size}) = Vector{UInt32}
_parameter_input_convert(::Type{<:Frame}, ::Val{:size}, x) = UInt32[x[1], x[2]]
_c_parameter_name(::Type{<:Frame}, ::Val{:size}) = "size"
_c_parameter_type_code(::Type{<:Frame}, ::Val{:size}) = ANARI_UINT32_VEC2

_parameter_arg_type(::Type{<:Frame}, ::Val{:accumulation}) = Bool
_parameter_input_type(::Type{<:Frame}, ::Val{:accumulation}) = Int32
_parameter_input_convert(::Type{<:Frame}, ::Val{:accumulation}, x) = Int32(x)
_c_parameter_name(::Type{<:Frame}, ::Val{:accumulation}) = "accumulation"
_c_parameter_type_code(::Type{<:Frame}, ::Val{:accumulation}) = ANARI_BOOL


function Frame{C}(dev::Device, r::Renderer, w::World, c::Camera; raw::Bool=false, kw...) where {C<:Colorant}
    h = anariNewFrame(dev.handle)
    h == C_NULL && error("unable to create frame")
    data = FrameData(;renderer=r, world=w, camera=c, kw...)
    f = Frame{C}(dev, h, data)
    if !raw
        anariSetParameter(dev.handle, f.handle, "channel.color", ANARI_DATA_TYPE, Ref(_ctypecode(C)))
        setalldata!(f, data)  # commit happens in here
    end
    f
end
Frame{C}(r::Renderer, w::World, c::Camera; kw...) where {C<:Colorant} = Frame(anariglobal(Device), r, w, c; kw...)

Frame(dev::Device, r::Renderer, w::World, c::Camera; kw...) = Frame{RGBA{N0f8}}(dev, r, w, c; kw...)
Frame(r::Renderer, w::World, c::Camera; kw...) = Frame(anariglobal(Device), r, w, c; kw...)

#TODO: not sure about this interface

function Base.isready(f::Frame; wait::Bool=false)
    Bool(anariFrameReady(Device(f).handle, f.handle, wait ? ANARI_WAIT : ANARI_NO_WAIT))
end

Base.wait(f::Frame) = (isready(f, wait=true); nothing)

render!(f::Frame) = (anariRenderFrame(Device(f).handle, f.handle); f)

function render!(f::Frame, n::Integer)
    for j ∈ 1:n
        render!(f)
        wait(f)
    end
    f
end

function image(f::Frame{C}) where {C}
    (x, y) = Ref{UInt32}.((0x00, 0x00))
    type = Ref{Int32}(ANARI_UNKNOWN)
    pxl = anariMapFrame(Device(f).handle, f.handle, "channel.color", x, y, type)
    cc = _ctypecode(C)
    if type[] ≠ cc
        error("got unexpected color type when getting frame image. code: $(type[]); expected: $cc")
    end
    o = copy(permutedims(unsafe_wrap(Array, convert(Ptr{C}, pxl), (x[], y[]))))
    anariUnmapFrame(Device(f).handle, f.handle, "channel.color")
    o
end
