
abstract type Geometry <: Object end

_ctypecode(::Type{<:Geometry}) = ANARI_GEOMETRY

Device(g::Geometry) = g.device

Base.show(io::IO, g::Geometry) = _defaultshow(io, g, ())

function translate!(g::Geometry, Δx::AbstractVector{<:Real}) 
    x = position(g)
    isnothing(x) && error("tried to translate geometry that has no position set")
    position!(g, x .+ Δx)
end


@kwdef mutable struct TrianglesData <: ObjectData
    position::Union{Nothing,Matrix{Float32}} = nothing

    # for now we are just going to enforce a specific color type here
    color::Union{Nothing,Vector{RGB{N0f8}}} = nothing

    index::Union{Nothing,Matrix{UInt32}} = nothing

    tangent::Union{Nothing,Matrix{Float32}} = nothing
    # this horrible thing is some terrible way of defining surface normals via the vertices
    # going to avoid it...
    normal::Union{Nothing,Matrix{Float32}} = nothing
end

mutable struct Triangles <: Geometry
    device::Device
    handle::ANARIGeometry
    data::TrianglesData

    function Triangles(dev::Device, h::ANARIGeometry, data::TrianglesData=TrianglesData())
        finalizer(new(dev, h, data)) do g
            anariRelease(dev.handle, g.handle)
        end
    end
end

associatedtype(::Type{<:TrianglesData}) = Triangles

subtype(g::Triangles) = :triangle


_parameter_arg_type(::Type{<:Triangles}, ::Val{:position}) = AbstractMatrix{<:Real}
_parameter_input_type(::Type{<:Triangles}, ::Val{:position}) = Matrix{Float32}
function _parameter_checks(::Type{<:Triangles}, ::Val{:position}, x::AbstractMatrix{<:Real})
    size(x,1) == 3 || throw(ArgumentError("anari only supports 3 spatial dimensions"))
end
_c_parameter_name(::Type{<:Triangles}, ::Val{:position}) = "vertex.position"
_parameter_container_type(::Type{<:Triangles}, ::Val{:position}) = Vector
_parameter_input_eltype(::Type{<:Triangles}, ::Val{:position}) = Float32
_c_parameter_eltype_code(::Type{<:Triangles}, ::Val{:position}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:Triangles}, ::Val{:color}) = AbstractVector{<:Colorant}
_parameter_input_type(::Type{<:Triangles}, ::Val{:color}) = RGBA{N0f8}
_c_parameter_name(::Type{<:Triangles}, ::Val{:color}) = "vertex.color"
_parameter_container_type(::Type{<:Triangles}, ::Val{:color}) = Vector
_parameter_input_eltype(::Type{<:Triangles}, ::Val{:color}) = RGBA{N0f8}
_c_parameter_eltype_code(::Type{<:Triangles}, ::Val{:color}) = _ctypecode(RGBA{N0f8})

_parameter_arg_type(::Type{<:Triangles}, ::Val{:index}) = AbstractMatrix{<:Integer}
_parameter_input_type(::Type{<:Triangles}, ::Val{:index}) = Matrix{UInt32}
_c_parameter_name(::Type{<:Triangles}, ::Val{:index}) = "primitive.index"
_parameter_container_type(::Type{<:Triangles}, ::Val{:index}) = Vector
_parameter_input_eltype(::Type{<:Triangles}, ::Val{:index}) = UInt32
_c_parameter_eltype_code(::Type{<:Triangles}, ::Val{:index}) = ANARI_UINT32_VEC3

_parameter_arg_type(::Type{<:Triangles}, ::Val{:tangent}) = AbstractMatrix{<:Real}
function _parameter_checks(::Type{<:Triangles}, ::Val{:tangent}, x::AbstractMatrix{<:Real})
    size(x,1) == 3 || throw(ArgumentError("anari only supports 3 spatial dimensions"))
end
_parameter_input_type(::Type{<:Triangles}, ::Val{:tangent}) = Matrix{Float32}
_c_parameter_name(::Type{<:Triangles}, ::Val{:tangent}) = "vertex.tangent"
_parameter_container_type(::Type{<:Triangles}, ::Val{:tangent}) = Vector
_parameter_input_eltype(::Type{<:Triangles}, ::Val{:tangent}) = Float32
_c_parameter_eltype_code(::Type{<:Triangles}, ::Val{:tangent}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:Triangles}, ::Val{:normal}) = AbstractMatrix{<:Real}
function _parameter_checks(::Type{<:Triangles}, ::Val{:normal}, x::AbstractMatrix{<:Real})
    size(x,1) == 3 || throw(ArgumentError("anari only supports 3 spatial dimensions"))
end
_parameter_input_type(::Type{<:Triangles}, ::Val{:normal}) = Matrix{Float32}
_c_parameter_name(::Type{<:Triangles}, ::Val{:normal}) = "vertex.normal"
_parameter_container_type(::Type{<:Triangles}, ::Val{:normal}) = Vector
_parameter_input_eltype(::Type{<:Triangles}, ::Val{:normal}) = Float32
_c_parameter_eltype_code(::Type{<:Triangles}, ::Val{:normal}) = ANARI_FLOAT32_VEC3


for (param, fname, fname!) ∈ [(:(:position), :position, :position!),
                              (:(:tangent), :tangent, :tangent!),
                              (:(:normal), :normal, :normal!),
                             ]
    @eval $fname(t::Triangles) = getparameter(t, Val{$param}())
    @eval function $fname!(t::Triangles, x::$(_parameter_arg_type(Triangles, Val{param.value}())); commit::Bool=true)
        setparameter!(t, Val{$param}(), x; commit)
        getparameter(t, Val{$param}())
    end
end

#TODO: would be nice to automatically set index by default
#not sure if there are any conventions

function Triangles(dev::Device, x::Union{Nothing,AbstractMatrix{<:Real}}=nothing; kw...)
    h = anariNewGeometry(dev.handle, "triangle")
    h == C_NULL && error("failed to create geometry")
    data = TrianglesData(;position=x, kw...)
    g = Triangles(dev, h, data)
    setalldata!(g, data)
    g
end
Triangles(x::Union{Nothing,AbstractMatrix{<:Real}}=nothing; kw...) = Triangles(anariglobal(Device), x; kw...)

function center(t::Triangles) 
    x = position(t)
    isnothing(x) && error("geometry has no position set")
    mean(eachcol(x))
end

#TODO: not too happy with the way this works

function rotate!(𝒻, t::Triangles)
    n = tangent(t)
    isnothing(n) || tangent!(t, 𝒻(n))
    n = normal(t)
    isnothing(n) || normal!(t, 𝒻(n))
    x = position(t)
    x₀ = center(t)
    t.data.position .= 𝒻(x .- x₀) .+ x₀
    setparameter!(t, Val{:position}())
end


@kwdef mutable struct SpheresData <: ObjectData
    position::Union{Nothing,Matrix{Float32}} = nothing
    radius::Union{Nothing,Vector{Float32}} = nothing
    color::Union{Nothing,Vector{RGBA{N0f8}}} = nothing
    index::Union{Nothing,Vector{UInt32}} = nothing
end

mutable struct Spheres <: Geometry
    device::Device
    handle::ANARIGeometry
    data::SpheresData

    function Spheres(dev::Device, h::ANARIGeometry, data::SpheresData=SpheresData())
        finalizer(new(dev, h, data)) do g
            anariRelease(dev.handle, g.handle)
        end
    end
end

associatedtype(::Type{<:SpheresData}) = Spheres

subtype(g::Spheres) = :sphere


_parameter_arg_type(::Type{<:Spheres}, ::Val{:position}) = AbstractMatrix{<:Real}
_parameter_input_type(::Type{<:Spheres}, ::Val{:position}) = Matrix{Float32}
function _parameter_checks(::Type{<:Spheres}, ::Val{:position}, x::AbstractMatrix{<:Real})
    size(x,1) == 3 || throw(ArgumentError("anari only supports 3 spatial dimensions"))
end
_c_parameter_name(::Type{<:Spheres}, ::Val{:position}) = "vertex.position"
_parameter_container_type(::Type{<:Spheres}, ::Val{:position}) = Vector
_parameter_input_eltype(::Type{<:Spheres}, ::Val{:position}) = Float32
_c_parameter_eltype_code(::Type{<:Spheres}, ::Val{:position}) = ANARI_FLOAT32_VEC3

_parameter_arg_type(::Type{<:Spheres}, ::Val{:radius}) = AbstractVector{<:Real}
_parameter_input_type(::Type{<:Spheres}, ::Val{:radius}) = Vector{Float32}
_c_parameter_name(::Type{<:Spheres}, ::Val{:radius}) = "vertex.radius"
_parameter_container_type(::Type{<:Spheres}, ::Val{:radius}) = Vector
_parameter_input_eltype(::Type{<:Spheres}, ::Val{:radius}) = Float32
_c_parameter_eltype_code(::Type{<:Spheres}, ::Val{:radius}) = ANARI_FLOAT32

_parameter_arg_type(::Type{<:Spheres}, ::Val{:color}) = AbstractVector{<:Colorant}
_parameter_input_type(::Type{<:Spheres}, ::Val{:color}) = RGBA{N0f8}
_c_parameter_name(::Type{<:Spheres}, ::Val{:color}) = "vertex.color"
_parameter_container_type(::Type{<:Spheres}, ::Val{:color}) = Vector
_parameter_input_eltype(::Type{<:Spheres}, ::Val{:color}) = RGBA{N0f8}
_c_parameter_eltype_code(::Type{<:Spheres}, ::Val{:color}) = _ctypecode(RGBA{N0f8})

_parameter_arg_type(::Type{<:Spheres}, ::Val{:index}) = AbstractMatrix{<:Integer}
_parameter_input_type(::Type{<:Spheres}, ::Val{:index}) = Matrix{UInt32}
_c_parameter_name(::Type{<:Spheres}, ::Val{:index}) = "primitive.index"
_parameter_container_type(::Type{<:Spheres}, ::Val{:index}) = Vector
_parameter_input_eltype(::Type{<:Spheres}, ::Val{:index}) = UInt32
_c_parameter_eltype_code(::Type{<:Spheres}, ::Val{:index}) = ANARI_UINT32_VEC3


for (param, fname, fname!) ∈ [(:(:position), :position, :position!),
                              (:(:radius), :radius, :radius!),
                             ]
    @eval $fname(s::Spheres) = getparameter(s, Val{$param}())
    @eval function $fname!(s::Spheres, x::$(_parameter_arg_type(Spheres, Val{param.value}())); commit::Bool=true)
        setparameter!(s, Val{$param}(), x; commit)
        getparameter(s, Val{$param}())
    end
end

function Spheres(dev::Device, x::Union{Nothing,AbstractMatrix{<:Real}}=nothing,
                 r::Union{Nothing,AbstractVector{<:Real}}=nothing;
                 kw...
                )
    h = anariNewGeometry(dev.handle, "sphere")
    h == C_NULL && error("failed to create geometry")
    data = SpheresData(;position=x, radius=r, kw...)
    s = Spheres(dev, h, data)
    setalldata!(s, data)
    s
end
function Spheres(x::Union{Nothing,AbstractMatrix{<:Real}}=nothing,
                 r::Union{Nothing,AbstractVector{<:Real}}=nothing;
                 kw...
                )
    Spheres(anariglobal(Device), x, r; kw...)
end
