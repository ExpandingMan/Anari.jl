
#TODO: this is very incomplete and largely a place-holder
# probably will not treat all the stuff world contains as parameters like we did for camera params

#WARN: the data here is all type unstable

@kwdef mutable struct WorldData <: ObjectData
    surfaces::Vector{Surface} = Surface[]
    lights::Vector{Light} = Light[]

    #TODO: instance and volume
end

mutable struct World <: Object
    device::Device
    handle::ANARIWorld
    data::WorldData

    function World(dev::Device, h::ANARIWorld, data::WorldData=WorldData())
        finalizer(new(dev, h, data)) do w
            #@debug("destroying World")
            anariRelease(dev.handle, w.handle)
        end
    end
end

associatedtype(::Type{WorldData}) = World

Device(w::World) = w.device

_parameter_arg_type(::Type{<:World}, ::Val{:surfaces}) = AbstractVector{Surface}
_parameter_input_type(::Type{<:World}, ::Val{:surfaces}) = Vector{Surface}
_c_parameter_name(::Type{<:World}, ::Val{:surfaces}) = "surface"
_parameter_container_type(::Type{<:World}, ::Val{:surfaces}) = Vector
_parameter_input_eltype(::Type{<:World}, ::Val{:surfaces}) = ANARISurface
_c_parameter_eltype_code(::Type{<:World}, ::Val{:surfaces}) = ANARI_SURFACE

_parameter_arg_type(::Type{<:World}, ::Val{:lights}) = AbstractVector{Light}
_paramter_input_type(::Type{<:World}, ::Val{:lights}) = Vector{Light}
_c_parameter_name(::Type{<:World}, ::Val{:lights}) = "light"
_parameter_container_type(::Type{<:World}, ::Val{:lights}) = Vector
_parameter_input_eltype(::Type{<:World}, ::Val{:lights}) = ANARILight
_c_parameter_eltype_code(::Type{<:World}, ::Val{:lights}) = ANARI_LIGHT


function World(dev::Device; raw::Bool=false, kw...)
    h = anariNewWorld(dev.handle)
    h == C_NULL && error("failed to create world")
    data = WorldData(;kw...)
    w = World(dev, h, data)
    raw || setalldata!(w, data)
    w
end
World(;kw...) = World(anariglobal(Device); kw...)
