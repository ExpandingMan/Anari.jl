
@kwdef mutable struct RendererData <: ObjectData
    ambient_color::RGB{Float32} = RGB{Float32}(1, 1, 1)
    ambient_radiance::Float32 = Float32(0)

    # this requires KHR_RENDERER_BACKGROUND_COLOR
    background::RGBA{Float32} = RGBA{Float32}(0,0,0,1)
end

mutable struct Renderer <: Object
    device::Device
    handle::ANARIRenderer
    subtype::Symbol
    data::RendererData

    function Renderer(dev::Device, h::ANARIRenderer,
                      subtype::Union{AbstractString,Symbol},
                      data::RendererData=RendererData(),
                     )
        finalizer(new(dev, h, Symbol(subtype), data)) do r
            #@debug("destroying Renderer")
            anariRelease(dev.handle, r.handle)
        end
    end
end

Device(r::Renderer) = r.device

subtype(r::Renderer) = r.subtype

Base.show(io::IO, r::Renderer) = _defaultshow(io, r, (:subtype,))

_ctypecode(::Type{Renderer}) = ANARI_RENDERER

_parameter_arg_type(::Type{<:Renderer}, ::Val{:ambient_color}) = Colorant
_parameter_input_type(::Type{<:Renderer}, ::Val{:ambient_color}) = Vector{Float32}
_c_parameter_name(::Type{<:Renderer}, ::Val{:ambient_color}) = "ambientColor"
_c_parameter_type_code(::Type{<:Renderer}, ::Val{:ambient_color}) = ANARI_FLOAT32_VEC3
function _parameter_input_convert(::Type{<:Renderer}, ::Val{:ambient_color}, c::Colorant)
    c = convert(RGB{Float32}, c)
    Float32[c.r, c.g, c.b]
end

_parameter_arg_type(::Type{<:Renderer}, ::Val{:ambient_radiance}) = Real
_parameter_input_type(::Type{<:Renderer}, ::Val{:ambient_radiance}) = Float32
function _parameter_checks(::Type{<:Renderer}, ::Val{:ambient_radiance}, x)
    x ≥ 0 || throw(ArgumentError("ambient radiance must be ≥ 0"))
end
_c_parameter_name(::Type{<:Renderer}, ::Val{:ambient_radiance}) = "ambientRadiance"
_c_parameter_type_code(::Type{<:Renderer}, ::Val{:ambient_radiance}) = ANARI_FLOAT32

_parameter_arg_type(::Type{<:Renderer}, ::Val{:background}) = Colorant
_parameter_input_type(::Type{<:Renderer}, ::Val{:background}) = Vector{Float32}
_c_parameter_name(::Type{<:Renderer}, ::Val{:background}) = "background"
_c_parameter_type_code(::Type{<:Renderer}, ::Val{:background}) = ANARI_FLOAT32_VEC4
function _parameter_input_convert(::Type{<:Renderer}, ::Val{:background}, c::Colorant)
    c = convert(RGBA{Float32}, c)
    Float32[c.r, c.g, c.b, c.alpha]
end

function Renderer(dev::Device, subtype::Symbol=defaultsubtype(dev, Renderer); raw::Bool=false, kw...)
    h = anariNewRenderer(dev.handle, string(subtype))
    h == C_NULL && error("failed to create renderer")
    data = RendererData(;kw...)
    r = Renderer(dev, h, subtype, data)
    raw || setalldata!(r, data)
    r
end
Renderer(;kw...) = Renderer(anariglobal(Device); kw...)
