module Anari

using Base: RefValue

using LinearAlgebra, Statistics
using StaticArrays
using FixedPointNumbers, Colors

using Infiltrator


include("utils.jl")
include("Lib.jl")
using .Lib

#TODO: the status callback can still cause Julia to complain that task switching is not allowed
#during finalizers, because libanari is allowed to call this on anariRelease.
#  So far I don't see a satisfactory way of solving this, but it can only produce error output when
#  debugging is on, so I'll live with it for now

function status_callback(data::Ptr{Cvoid}, dev::ANARIDevice, src::ANARIObject, type::ANARIDataType,
                         severity::ANARIStatusSeverity, code::ANARIStatusCode,
                         msg::Cstring,
                        )
    msg = unsafe_string(msg)
    if severity == ANARI_SEVERITY_FATAL_ERROR
        @error("ANARI: fatal error")
        error(msg)
    elseif severity == ANARI_SEVERITY_ERROR
        @error("ANARI: error")
        error(msg)
    elseif severity ∈ (ANARI_SEVERITY_WARNING, ANARI_SEVERITY_PERFORMANCE_WARNING)
        @warn("ANARI:  $msg")
    elseif severity == ANARI_SEVERITY_INFO
        @info("ANARI:  $msg")
    elseif severity == ANARI_SEVERITY_DEBUG
        @debug("ANARI:  $msg")
    else
        @warn("unrecognized ANARI severity level: $severity")
        @warn("ANARI:  $msg")
    end
end


include("objects.jl")
include("device.jl")
include("sampler.jl")
include("renderer.jl")
include("camera.jl")
include("material.jl")
include("light.jl")
include("geometry.jl")
include("surface.jl")
include("world.jl")
include("frame.jl")


#TODO: not really sure we want a global device by default yet
function __init__()
    anariglobal!(Library(:visrtx))
    anariglobal!(Device(LIBRARY[]))
end


export anariglobal, anariglobal!

end
