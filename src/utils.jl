
"""
    _unsafe_load_string_array(ptr::Ptr)

ANARI libraries sometimes return arrays of strings in the form of a null-terminated array of string
pointers.  This function should correctly load those into `Vector{String}`
"""
function _unsafe_load_string_array(ptr::Ptr{P}) where {P}
    o = String[]
    while true
        p = unsafe_load(ptr)
        p == C_NULL && break
        s = unsafe_string(p)
        ptr += sizeof(P)
        push!(o, unsafe_string(p))
    end
    o
end

"""
    _unsafe_load_param_array(ptr::Ptr)

The function `anariGetObjectInfo` returns an array of `ANARIParameter` structs such that the termination
of the array is signalled by a null pointer in the `name` field.  This seems incredibly weird and unsafe to me.
This function should load those correctly as a `Vector{ANARIParameter}`.  Note that the names will need to
be converted to strings via `unsafe_string`.
"""
function _unsafe_load_param_array(ptr::Ptr)
    o = ANARIParameter[]
    while true
        p = unsafe_load(convert(Ptr{ANARIParameter}, ptr))
        p.name == C_NULL && break
        push!(o, p)
        ptr += sizeof(ANARIParameter)
    end
    o
end

function _defaultshow(io::IO, x, fields=fieldnames(typeof(x)))
    show(io, typeof(x))
    print(io, "(")
    for j ∈ 1:length(fields)
        show(io, getfield(x, fields[j]))
        j < length(fields) && print(io, ", ")
    end
    print(io, ")")
end

_non_nothing_type(::Type{T}) where {T} = T
_non_nothing_type(::Type{Union{T,Nothing}}) where {T} = T
