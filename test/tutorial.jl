#====================================================================================================
       Tutorial

This is the Julia version of the simple tutorial found in the anari SDK repo.
It was done prior to creating most abstractions so it follows the C example fairly closely.

TODO: this more or less seems to be working, but surface is always rendering as all black...
Not sure if this is to be expected or not, light doesn't have any parameters set.
====================================================================================================#
using Anari
using Anari.Lib
using Transducers
using Colors
using Images, KittyTerminalImages

using Colors: N0f8
using Anari: Library, Device, Renderer

ENV["JULIA_DEBUG"] = "Anari"


function get_param_info_description(dev::Device, nt::NamedTuple)
    ptr = anariGetParameterInfo(dev.handle, ANARI_RENDERER, dev.type, nt.name, nt.type, "description", ANARI_STRING)
    ptr == C_NULL ? nothing : unsafe_string(convert(Ptr{Cchar}, ptr))
end

function get_param_info_required(dev::Device, nt::NamedTuple)
    ptr = anariGetParameterInfo(dev.handle, ANARI_RENDERER, dev.type, nt.name, nt.type, "required", ANARI_BOOL)
    ptr == C_NULL ? false : Bool(unsafe_load(convert(Ptr{Cint}, ptr)))
end

function get_frame(dev::Device, frame::ANARIFrame)
    (x, y) = Ref{UInt32}.((0x00, 0x00))
    type = Ref{Int32}(ANARI_UNKNOWN)
    pixel = anariMapFrame(dev.handle, frame, "channel.color", x, y, type)
    (type[] == ANARI_UFIXED8_RGBA_SRGB) || error("unexpected color type $type")
    o = copy(permutedims(unsafe_wrap(Array, convert(Ptr{RGBA{N0f8}}, pixel), (x[], y[]))))
    anariRelease(dev.handle, frame)
    o
end

const dev = anariglobal(Device)

imgsize() = Int32[1024, 768]

campos() = Float32[0, 0, 1]
camup() = Float32[0, 1, 0]
camview() = Float32[0, 0, -1]

vertex() = Float32[-0.5  0.5 -0.5 0.5
                   -0.5 -0.5  0.5 0.5
                      0    0    0   0
                  ]

color() = RGBA{N0f8}[colorant"rgba(255,0,0,1)",
                     colorant"rgba(0,0,255,1)",
                     colorant"rgba(0,255,0,1)",
                     colorant"rgba(0,0,255,1)",
                    ]

index() = Int32[0 1
                1 2
                2 3
               ]

const FB_FORMAT = ANARI_UFIXED8_RGBA_SRGB

function main(N=10, dev=dev)
    rndrs = Anari._unsafe_load_string_array(anariGetObjectSubtypes(dev.handle, ANARI_RENDERER))

    isempty(rndrs) && error("no renderers found")

    ptr = anariGetObjectInfo(dev.handle, ANARI_RENDERER, "default", "parameter", ANARI_PARAMETER_LIST)
    params = Anari._unsafe_load_param_array(ptr)
    params = params |> Map(x -> (name=unsafe_string(x.name), type=x.type)) |> collect

    # does this do anything here? don't think so
    anariCommitParameters(dev.handle, dev.handle)

    camera = anariNewCamera(dev.handle, "perspective")

    aspect = Float32(imgsize()[1]/imgsize()[2])

    (pos, up, vw) = (campos(), camup(), camview())
    GC.@preserve pos up vw begin
        anariSetParameter(dev.handle, camera, "aspect", ANARI_FLOAT32, Ref(aspect))
        anariSetParameter(dev.handle, camera, "position", ANARI_FLOAT32_VEC3, pos)
        anariSetParameter(dev.handle, camera, "direction", ANARI_FLOAT32_VEC3, vw)
        anariSetParameter(dev.handle, camera, "up", ANARI_FLOAT32_VEC3, up)
        anariCommitParameters(dev.handle, camera)
    end

    world = anariNewWorld(dev.handle)

    mesh = anariNewGeometry(dev.handle, "triangle")

    # note that anariSetParameter takes pointers to pointers returned by array creation
    # it complains that it is making a private copy of these, so you probably want to
    # do this differently in real life

    vtx = vertex()
    GC.@preserve vtx begin
        # NOTE: this is the ELEMENT type, so in this case an array of 4
        array = anariNewArray1D(dev.handle, vtx, C_NULL, C_NULL, ANARI_FLOAT32_VEC3, 4)
        anariCommitParameters(dev.handle, array)
        anariSetParameter(dev.handle, mesh, "vertex.position", ANARI_ARRAY1D, Ref(array))
        anariRelease(dev.handle, array)
    end

    # triangle vertex colors
    clr = color()
    GC.@preserve clr begin
        array = anariNewArray1D(dev.handle, clr, C_NULL, C_NULL, ANARI_UFIXED8_VEC4, 4)
        anariCommitParameters(dev.handle, array)
        anariSetParameter(dev.handle, mesh, "vertex.color", ANARI_ARRAY1D, Ref(array))
        anariRelease(dev.handle, array)
    end

    # set triangle indices
    idx = index()
    GC.@preserve idx begin
        array = anariNewArray1D(dev.handle, idx, C_NULL, C_NULL, ANARI_UINT32_VEC3, 2)
        anariCommitParameters(dev.handle, array)
        anariSetParameter(dev.handle, mesh, "primitive.index", ANARI_ARRAY1D, Ref(array))
        anariRelease(dev.handle, array)
    end

    anariCommitParameters(dev.handle, mesh)

    mat = anariNewMaterial(dev.handle, "matte")
    (mode, col) = ("blend", "color")
    GC.@preserve mode col begin
        anariSetParameter(dev.handle, mat, "color", ANARI_STRING, Ref(col))
        anariSetParameter(dev.handle, mat, "alphaMode", ANARI_STRING, Ref(mode))
        anariCommitParameters(dev.handle, mat)
    end

    surface = anariNewSurface(dev.handle)
    anariSetParameter(dev.handle, surface, "geometry", ANARI_GEOMETRY, Ref(mesh))
    anariSetParameter(dev.handle, surface, "material", ANARI_MATERIAL, Ref(mat))
    anariCommitParameters(dev.handle, surface)
    anariRelease(dev.handle, mesh)
    anariRelease(dev.handle, mat)

    array = anariNewArray1D(dev.handle, Ref(surface), C_NULL, C_NULL, ANARI_SURFACE, 1)
    anariCommitParameters(dev.handle, array)
    anariSetParameter(dev.handle, world, "surface", ANARI_ARRAY1D, Ref(array))
    anariRelease(dev.handle, surface)
    anariRelease(dev.handle, array)

    light = anariNewLight(dev.handle, "point")
    light_power = Float32(20.0)
    anariSetParameter(dev.handle, light, "power", ANARI_FLOAT32, Ref(light_power))
    anariCommitParameters(dev.handle, light)
    array = anariNewArray1D(dev.handle, Ref(light), C_NULL, C_NULL, ANARI_LIGHT, 1)
    anariCommitParameters(dev.handle, array)
    anariSetParameter(dev.handle, world, "light", ANARI_ARRAY1D, Ref(array))
    anariRelease(dev.handle, light)
    anariRelease(dev.handle, array)

    anariCommitParameters(dev.handle, world)

    world_bounds = zeros(Float32, 6)
    ok = anariGetProperty(dev.handle, world, "bounds", ANARI_FLOAT32_BOX3,
                          world_bounds, sizeof(world_bounds), ANARI_WAIT,
                         )
    Bool(ok) || error("failed to get world bounds")
    @info("world bounds:", world_bounds)

    renderer = anariNewRenderer(dev.handle, "default")
    bgcolor = Float32[1.0, 1.0, 1.0, 1.0]
    GC.@preserve bgcolor begin
        anariSetParameter(dev.handle, renderer, "background", ANARI_FLOAT32_VEC4, bgcolor)
        anariCommitParameters(dev.handle, renderer)
    end

    frame = anariNewFrame(dev.handle)

    img_size = imgsize()
    GC.@preserve img_size begin
        anariSetParameter(dev.handle, frame, "size", ANARI_UINT32_VEC2, img_size)
        anariSetParameter(dev.handle, frame, "channel.color", ANARI_DATA_TYPE, Ref(FB_FORMAT))
        anariSetParameter(dev.handle, frame, "renderer", ANARI_RENDERER, Ref(renderer))
        anariSetParameter(dev.handle, frame, "camera", ANARI_CAMERA, Ref(camera))
        anariSetParameter(dev.handle, frame, "world", ANARI_WORLD, Ref(world))
        anariCommitParameters(dev.handle, frame)
    end

    # frame rendering
    for j ∈ 1:N
        anariRenderFrame(dev.handle, frame)
        anariFrameReady(dev.handle, frame, ANARI_WAIT)
    end

    o = get_frame(dev, frame)

    anariRelease(dev.handle, renderer)
    anariRelease(dev.handle, camera)
    anariRelease(dev.handle, world)

    o
end

