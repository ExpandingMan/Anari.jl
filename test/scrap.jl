using Anari
using Anari.Lib
using Transducers
using Colors, StaticArrays, Rotations
using Images, KittyTerminalImages


using Colors: N0f8
using Anari: Library, Device, Renderer, PerspectiveCamera, Camera, World, Frame, Surface, PointLight, DirectionalLight
using Anari: Image1D
using Anari: Triangles, Spheres
using Anari: Matte, PhysicallyBased
using Anari: position, position!, getparameter, setparameter!, render!, image, translate!, rotate!

ENV["JULIA_DEBUG"] = "Anari"


#YAY! now seems to be basically working!!!

const dev = anariglobal(Device)

tutorial() = quote
    c = PerspectiveCamera([0,0,2], [0,0,-1], [0,1,0])

    r = Renderer(background=RGB(1,1,1))#, ambient_radiance=10)

    img = Image1D([RGB(1,0,0), RGB(0,0,1)])

    s = Spheres([0; 0; 0;;], [0.2])

    m = Matte(color=img)
    #m = PhysicallyBased(base_color=RGB(1,0,0))

    σ = Surface(s, m)

    #l = PointLight(power=5)
    l = DirectionalLight([0,0,-1], irradiance=0.5)

    w = World(surfaces=[σ], lights=[l])

    f = Frame(r, w, c, size=(1024,1024))

    render!(f, 1)
    fimg = image(f)
end


#====================================================================================================
       trying to work backward from tutorial to see wtf goes wrong
====================================================================================================#


const FB_FORMAT = ANARI_UFIXED8_RGBA_SRGB


function get_frame(dev::Device, frame::ANARIFrame)
    (x, y) = Ref{UInt32}.((0x00, 0x00))
    type = Ref{Int32}(ANARI_UNKNOWN)
    pixel = anariMapFrame(dev.handle, frame, "channel.color", x, y, type)
    (type[] == ANARI_UFIXED8_RGBA_SRGB) || error("unexpected color type $type")
    o = copy(permutedims(unsafe_wrap(Array, convert(Ptr{RGBA{N0f8}}, pixel), (x[], y[]))))
    anariRelease(dev.handle, frame)
    o
end



function debug2()
    c = PerspectiveCamera();

    w = World()

    r = Renderer()

    #frame = anariNewFrame(dev.handle)
    f = Frame(r, w, c; raw=false)

    render!(f)

    image(f)
end


