# Anari

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Anari.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/Anari.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/Anari.jl/-/pipelines)
[![Build Status](https://github.com/ExpandingMan/Anari.jl/actions/workflows/CI.yml/badge.svg?branch=main)](https://github.com/ExpandingMan/Anari.jl/actions/workflows/CI.yml?query=branch%3Amain)

Julia wrapper for the [ANARI](https://www.khronos.org/anari) 3D rendering engine API.

Still in early development.

This requires [ANARI-SDK](https://github.com/KhronosGroup/ANARI-SDK) and
[VisRTX](https://github.com/NVIDIA/VisRTX) built as JLL's.  So far I'm only attempting to
provide support for the NVidia VisRTX implementation.


## ⚠ Status ⚠
Currently it is a problem to build the nvidia implementation (VisRTX) of ANARI because it depends on
the proprietary `OptiX` which is locked behind a login barrier.  Therefore, I'll do no further
development of this until there is an alternative.

It is possible to build this package by creating your own JLL's with
[BinaryBuilder.jl](https://github.com/JuliaPackaging/BinaryBuilder.jl).

- [`build_tarballs.jl` for VisRTX](https://gitlab.com/-/snippets/3597614) (requires OptiX headers,
    see [VisRTX](https://github.com/NVIDIA/VisRTX)).
- [`build_tarballs.jl` for ANARI-SDK](https://gitlab.com/-/snippets/3597613)
