using Anari
using Documenter

DocMeta.setdocmeta!(Anari, :DocTestSetup, :(using Anari); recursive=true)

makedocs(;
    modules=[Anari],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Anari.jl/blob/{commit}{path}#{line}",
    sitename="Anari.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Anari.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
