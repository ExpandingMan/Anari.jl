```@meta
CurrentModule = Anari
```

# Anari

Documentation for [Anari](https://gitlab.com/ExpandingMan/Anari.jl).

```@index
```

```@autodocs
Modules = [Anari]
```
